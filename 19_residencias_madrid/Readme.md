Este archivo incluye las sanciones impuestas a las residencias de ancianos y centros de día de la Comunidad de Madrid entre 2014 y 2018. 
Se incluye el nombre de la empresa que gestiona cada centro, el centro sancionado, el número de expediente, los artículos de la ley inflingidos y la cuantía de la sanción. 
Las sanciones son motivadas por infracciones contenidas en la [ley autonómica 11/2002 de Ordenación de la Actividad de los Centros y Servicios de Acción Social](https://www.boe.es/buscar/pdf/2003/BOE-A-2003-4505-consolidado.pdf)

La información ha sido obtenida por el diario El País tras una petición de acceso a la información realizada utilizando la Ley de Transparencia.