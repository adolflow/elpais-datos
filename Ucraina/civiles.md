Hoja de cálculo con las geolocalizaciones de vídeos y fotografías analizados por EL PAÍS
entre el 28 de febrero y el 4 de marzo de 2022

Datos: https://docs.google.com/spreadsheets/u/2/d/19Hhs47PDuIrO7rgYLVzfoISXf0SSag84-LJ1HJw3wiU/edit#gid=0

Artículo: 
https://elpais.com/internacional/2022-03-06/bloques-de-pisos-una-universidad-y-una-guarderia-cinco-blancos-civiles-de-la-guerra-de-putin-en-ucrania.html
