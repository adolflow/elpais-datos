# Diputadxs del Congreso
 
El CSV incluye los nombres, el sexo y la fecha de nacimiento de lxs diputadxs del Congreso que han empezado las legislatura de la democracia española, de la constituyente hasta la XV. 
Los datos de lxs diputadxs de la primera hasta la sexta legislatura han sido recopilados manualmente acudiendo al archivo del Congreso. A partir de la VII legislatura, la información procedo de la [página web del Congreso](https://www.congreso.es/es/)

## Primera exploración

El archivo [congreso_espana_nacimiento_diputados.csv](congreso_espana_nacimiento_diputados.csv) no se mostraba bien en la tabla que renderiza Gitlab por algunos caracteres.

Con un `file` salía:

```
$ file congreso_espana_nacimiento_diputados.csv 
congreso_espana_nacimiento_diputados.csv: ISO-8859 text
```

Al hacer un `iconv` para pasarlo a `UTF-8` daba error:

```
$ iconv -f UTF-8 congreso_espana_nacimiento_diputados.csv -o congreso_nacimiento_diputadxs.csv
iconv: secuencia de entrada ilegal en la posición 82
```

Qué pasa en esa posición (línea) 82:
```
$ head -82 congreso_espana_nacimiento_diputados.csv | tail -1
De Andr�s Guerra, Javier;�lava;PP;15;03/10/1967;H
```

Utilizo otra aplicación a ver si me "canta" algo distinto. Al intentar abrirlo con LibreOffice me dice que detecta la codificación `TURCO (ISO-8859-9)`.
Así que uso esa codificación de entrada del archivo para convertirlo a UTF-8 de salida:

```
$ iconv -f ISO-8859-9 -t UTF-8 congreso_espana_nacimiento_diputados.csv -o congreso_nacimiento_diputadxs.csv
```

No da error por lo que voy a ver qué tal este archivo con `file`, la misma fila que daba error con `head` y `tail`y el número de líneas con `wc`:

```
$ file congreso_nacimiento_diputadxs.csv 
congreso_nacimiento_diputadxs.csv: Unicode text, UTF-8 text
$ head -82 congreso_nacimiento_diputadxs.csv | tail -1
De Andrés Guerra, Javier;Álava;PP;15;03/10/1967;H
$ wc -l congreso_nacimiento_diputadxs.csv
6262 congreso_nacimiento_diputadxs.csv
```

## Segunda exploración

To be continued...

